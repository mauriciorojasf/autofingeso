/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsf;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author christofher
 */
@ManagedBean
@SessionScoped
public class Usuario {
    
    private String nombre;
    
    public String getNombre(){
    
        return nombre;
    }
    
    public void setNombre(String nombre1){
    
            this.nombre = nombre1;
    
   
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EntityClasses;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mauricio
 */
@Entity
@Table(name = "orden compra")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OrdenCompra.findAll", query = "SELECT o FROM OrdenCompra o"),
    @NamedQuery(name = "OrdenCompra.findByIdCompra", query = "SELECT o FROM OrdenCompra o WHERE o.idCompra = :idCompra"),
    @NamedQuery(name = "OrdenCompra.findByIdCliente", query = "SELECT o FROM OrdenCompra o WHERE o.idCliente = :idCliente"),
    @NamedQuery(name = "OrdenCompra.findByIdVendedor", query = "SELECT o FROM OrdenCompra o WHERE o.idVendedor = :idVendedor"),
    @NamedQuery(name = "OrdenCompra.findByMontoCompra", query = "SELECT o FROM OrdenCompra o WHERE o.montoCompra = :montoCompra"),
    @NamedQuery(name = "OrdenCompra.findByIdAuto", query = "SELECT o FROM OrdenCompra o WHERE o.idAuto = :idAuto")})
public class OrdenCompra implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "IdCompra")
    private String idCompra;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "IdCliente")
    private String idCliente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "IdVendedor")
    private String idVendedor;
    @Basic(optional = false)
    @NotNull
    @Column(name = "MontoCompra")
    private int montoCompra;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "IdAuto")
    private String idAuto;

    public OrdenCompra() {
    }

    public OrdenCompra(String idCompra) {
        this.idCompra = idCompra;
    }

    public OrdenCompra(String idCompra, String idCliente, String idVendedor, int montoCompra, String idAuto) {
        this.idCompra = idCompra;
        this.idCliente = idCliente;
        this.idVendedor = idVendedor;
        this.montoCompra = montoCompra;
        this.idAuto = idAuto;
    }

    public String getIdCompra() {
        return idCompra;
    }

    public void setIdCompra(String idCompra) {
        this.idCompra = idCompra;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getIdVendedor() {
        return idVendedor;
    }

    public void setIdVendedor(String idVendedor) {
        this.idVendedor = idVendedor;
    }

    public int getMontoCompra() {
        return montoCompra;
    }

    public void setMontoCompra(int montoCompra) {
        this.montoCompra = montoCompra;
    }

    public String getIdAuto() {
        return idAuto;
    }

    public void setIdAuto(String idAuto) {
        this.idAuto = idAuto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCompra != null ? idCompra.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrdenCompra)) {
            return false;
        }
        OrdenCompra other = (OrdenCompra) object;
        if ((this.idCompra == null && other.idCompra != null) || (this.idCompra != null && !this.idCompra.equals(other.idCompra))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "EntityClasses.OrdenCompra[ idCompra=" + idCompra + " ]";
    }
    
}

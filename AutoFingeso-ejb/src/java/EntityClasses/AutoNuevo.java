/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EntityClasses;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mauricio
 */
@Entity
@Table(name = "auto nuevo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AutoNuevo.findAll", query = "SELECT a FROM AutoNuevo a"),
    @NamedQuery(name = "AutoNuevo.findByTipo", query = "SELECT a FROM AutoNuevo a WHERE a.tipo = :tipo"),
    @NamedQuery(name = "AutoNuevo.findByMarca", query = "SELECT a FROM AutoNuevo a WHERE a.marca = :marca"),
    @NamedQuery(name = "AutoNuevo.findByA\u00f1o", query = "SELECT a FROM AutoNuevo a WHERE a.a\u00f1o = :a\u00f1o"),
    @NamedQuery(name = "AutoNuevo.findByModelo", query = "SELECT a FROM AutoNuevo a WHERE a.modelo = :modelo"),
    @NamedQuery(name = "AutoNuevo.findByDescripci\u00f3n", query = "SELECT a FROM AutoNuevo a WHERE a.descripci\u00f3n = :descripci\u00f3n"),
    @NamedQuery(name = "AutoNuevo.findByIdAutoNuevo", query = "SELECT a FROM AutoNuevo a WHERE a.idAutoNuevo = :idAutoNuevo"),
    @NamedQuery(name = "AutoNuevo.findByStock", query = "SELECT a FROM AutoNuevo a WHERE a.stock = :stock"),
    @NamedQuery(name = "AutoNuevo.findByPrecio", query = "SELECT a FROM AutoNuevo a WHERE a.precio = :precio")})
public class AutoNuevo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "Tipo")
    private String tipo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "Marca")
    private String marca;
    @Basic(optional = false)
    @NotNull
    @Column(name = "A\u00f1o")
    private int año;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "Modelo")
    private String modelo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "Descripci\u00f3n")
    private String descripción;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "IdAutoNuevo")
    private String idAutoNuevo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Stock")
    private int stock;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Precio")
    private int precio;

    public AutoNuevo() {
    }

    public AutoNuevo(String idAutoNuevo) {
        this.idAutoNuevo = idAutoNuevo;
    }

    public AutoNuevo(String idAutoNuevo, String tipo, String marca, int año, String modelo, String descripción, int stock, int precio) {
        this.idAutoNuevo = idAutoNuevo;
        this.tipo = tipo;
        this.marca = marca;
        this.año = año;
        this.modelo = modelo;
        this.descripción = descripción;
        this.stock = stock;
        this.precio = precio;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public int getAño() {
        return año;
    }

    public void setAño(int año) {
        this.año = año;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getDescripción() {
        return descripción;
    }

    public void setDescripción(String descripción) {
        this.descripción = descripción;
    }

    public String getIdAutoNuevo() {
        return idAutoNuevo;
    }

    public void setIdAutoNuevo(String idAutoNuevo) {
        this.idAutoNuevo = idAutoNuevo;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAutoNuevo != null ? idAutoNuevo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AutoNuevo)) {
            return false;
        }
        AutoNuevo other = (AutoNuevo) object;
        if ((this.idAutoNuevo == null && other.idAutoNuevo != null) || (this.idAutoNuevo != null && !this.idAutoNuevo.equals(other.idAutoNuevo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "EntityClasses.AutoNuevo[ idAutoNuevo=" + idAutoNuevo + " ]";
    }
    
}

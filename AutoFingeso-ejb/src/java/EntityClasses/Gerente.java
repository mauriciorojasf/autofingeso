/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EntityClasses;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mauricio
 */
@Entity
@Table(name = "gerente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Gerente.findAll", query = "SELECT g FROM Gerente g"),
    @NamedQuery(name = "Gerente.findByIdGerente", query = "SELECT g FROM Gerente g WHERE g.idGerente = :idGerente"),
    @NamedQuery(name = "Gerente.findByIdSucursal", query = "SELECT g FROM Gerente g WHERE g.idSucursal = :idSucursal"),
    @NamedQuery(name = "Gerente.findByNombre", query = "SELECT g FROM Gerente g WHERE g.nombre = :nombre"),
    @NamedQuery(name = "Gerente.findByApellido", query = "SELECT g FROM Gerente g WHERE g.apellido = :apellido"),
    @NamedQuery(name = "Gerente.findByRut", query = "SELECT g FROM Gerente g WHERE g.rut = :rut"),
    @NamedQuery(name = "Gerente.findByTel\u00e9fono", query = "SELECT g FROM Gerente g WHERE g.tel\u00e9fono = :tel\u00e9fono"),
    @NamedQuery(name = "Gerente.findByCorreo", query = "SELECT g FROM Gerente g WHERE g.correo = :correo"),
    @NamedQuery(name = "Gerente.findByContrase\u00f1a", query = "SELECT g FROM Gerente g WHERE g.contrase\u00f1a = :contrase\u00f1a")})
public class Gerente implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "IdGerente")
    private String idGerente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "IdSucursal")
    private String idSucursal;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "Nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "Apellido")
    private String apellido;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "RUT")
    private String rut;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Tel\u00e9fono")
    private int teléfono;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "Correo")
    private String correo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "Contrase\u00f1a")
    private String contraseña;

    public Gerente() {
    }

    public Gerente(String idGerente) {
        this.idGerente = idGerente;
    }

    public Gerente(String idGerente, String idSucursal, String nombre, String apellido, String rut, int teléfono, String correo, String contraseña) {
        this.idGerente = idGerente;
        this.idSucursal = idSucursal;
        this.nombre = nombre;
        this.apellido = apellido;
        this.rut = rut;
        this.teléfono = teléfono;
        this.correo = correo;
        this.contraseña = contraseña;
    }

    public String getIdGerente() {
        return idGerente;
    }

    public void setIdGerente(String idGerente) {
        this.idGerente = idGerente;
    }

    public String getIdSucursal() {
        return idSucursal;
    }

    public void setIdSucursal(String idSucursal) {
        this.idSucursal = idSucursal;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public int getTeléfono() {
        return teléfono;
    }

    public void setTeléfono(int teléfono) {
        this.teléfono = teléfono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idGerente != null ? idGerente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Gerente)) {
            return false;
        }
        Gerente other = (Gerente) object;
        if ((this.idGerente == null && other.idGerente != null) || (this.idGerente != null && !this.idGerente.equals(other.idGerente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "EntityClasses.Gerente[ idGerente=" + idGerente + " ]";
    }
    
}

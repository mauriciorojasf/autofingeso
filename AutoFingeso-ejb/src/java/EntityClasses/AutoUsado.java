/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EntityClasses;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mauricio
 */
@Entity
@Table(name = "auto usado")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AutoUsado.findAll", query = "SELECT a FROM AutoUsado a"),
    @NamedQuery(name = "AutoUsado.findByTipo", query = "SELECT a FROM AutoUsado a WHERE a.tipo = :tipo"),
    @NamedQuery(name = "AutoUsado.findByMarca", query = "SELECT a FROM AutoUsado a WHERE a.marca = :marca"),
    @NamedQuery(name = "AutoUsado.findByA\u00f1o", query = "SELECT a FROM AutoUsado a WHERE a.a\u00f1o = :a\u00f1o"),
    @NamedQuery(name = "AutoUsado.findByModelo", query = "SELECT a FROM AutoUsado a WHERE a.modelo = :modelo"),
    @NamedQuery(name = "AutoUsado.findByDescripci\u00f3n", query = "SELECT a FROM AutoUsado a WHERE a.descripci\u00f3n = :descripci\u00f3n"),
    @NamedQuery(name = "AutoUsado.findByIdAutoUsado", query = "SELECT a FROM AutoUsado a WHERE a.idAutoUsado = :idAutoUsado"),
    @NamedQuery(name = "AutoUsado.findByIdDue\u00f1oAnterior", query = "SELECT a FROM AutoUsado a WHERE a.idDue\u00f1oAnterior = :idDue\u00f1oAnterior"),
    @NamedQuery(name = "AutoUsado.findByMonto", query = "SELECT a FROM AutoUsado a WHERE a.monto = :monto")})
public class AutoUsado implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "Tipo")
    private String tipo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "Marca")
    private String marca;
    @Basic(optional = false)
    @NotNull
    @Column(name = "A\u00f1o")
    private int año;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "Modelo")
    private String modelo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "Descripci\u00f3n")
    private String descripción;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "IdAutoUsado")
    private String idAutoUsado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "IdDue\u00f1oAnterior")
    private String idDueñoAnterior;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Monto")
    private int monto;

    public AutoUsado() {
    }

    public AutoUsado(String idAutoUsado) {
        this.idAutoUsado = idAutoUsado;
    }

    public AutoUsado(String idAutoUsado, String tipo, String marca, int año, String modelo, String descripción, String idDueñoAnterior, int monto) {
        this.idAutoUsado = idAutoUsado;
        this.tipo = tipo;
        this.marca = marca;
        this.año = año;
        this.modelo = modelo;
        this.descripción = descripción;
        this.idDueñoAnterior = idDueñoAnterior;
        this.monto = monto;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public int getAño() {
        return año;
    }

    public void setAño(int año) {
        this.año = año;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getDescripción() {
        return descripción;
    }

    public void setDescripción(String descripción) {
        this.descripción = descripción;
    }

    public String getIdAutoUsado() {
        return idAutoUsado;
    }

    public void setIdAutoUsado(String idAutoUsado) {
        this.idAutoUsado = idAutoUsado;
    }

    public String getIdDueñoAnterior() {
        return idDueñoAnterior;
    }

    public void setIdDueñoAnterior(String idDueñoAnterior) {
        this.idDueñoAnterior = idDueñoAnterior;
    }

    public int getMonto() {
        return monto;
    }

    public void setMonto(int monto) {
        this.monto = monto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAutoUsado != null ? idAutoUsado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AutoUsado)) {
            return false;
        }
        AutoUsado other = (AutoUsado) object;
        if ((this.idAutoUsado == null && other.idAutoUsado != null) || (this.idAutoUsado != null && !this.idAutoUsado.equals(other.idAutoUsado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "EntityClasses.AutoUsado[ idAutoUsado=" + idAutoUsado + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EntityClasses;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mauricio
 */
@Entity
@Table(name = "vendedor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vendedor.findAll", query = "SELECT v FROM Vendedor v"),
    @NamedQuery(name = "Vendedor.findByIdVendedor", query = "SELECT v FROM Vendedor v WHERE v.idVendedor = :idVendedor"),
    @NamedQuery(name = "Vendedor.findByIdSucursal", query = "SELECT v FROM Vendedor v WHERE v.idSucursal = :idSucursal"),
    @NamedQuery(name = "Vendedor.findByNombre", query = "SELECT v FROM Vendedor v WHERE v.nombre = :nombre"),
    @NamedQuery(name = "Vendedor.findByApellido", query = "SELECT v FROM Vendedor v WHERE v.apellido = :apellido"),
    @NamedQuery(name = "Vendedor.findByRut", query = "SELECT v FROM Vendedor v WHERE v.rut = :rut"),
    @NamedQuery(name = "Vendedor.findByTel\u00e9fono", query = "SELECT v FROM Vendedor v WHERE v.tel\u00e9fono = :tel\u00e9fono"),
    @NamedQuery(name = "Vendedor.findByCorreo", query = "SELECT v FROM Vendedor v WHERE v.correo = :correo"),
    @NamedQuery(name = "Vendedor.findByAutosVendidos", query = "SELECT v FROM Vendedor v WHERE v.autosVendidos = :autosVendidos"),
    @NamedQuery(name = "Vendedor.findByContrase\u00f1a", query = "SELECT v FROM Vendedor v WHERE v.contrase\u00f1a = :contrase\u00f1a")})
public class Vendedor implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "IdVendedor")
    private String idVendedor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "IdSucursal")
    private String idSucursal;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "Nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "Apellido")
    private String apellido;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "RUT")
    private String rut;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Tel\u00e9fono")
    private int teléfono;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "Correo")
    private String correo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AutosVendidos")
    private int autosVendidos;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "Contrase\u00f1a")
    private String contraseña;

    public Vendedor() {
    }

    public Vendedor(String idVendedor) {
        this.idVendedor = idVendedor;
    }

    public Vendedor(String idVendedor, String idSucursal, String nombre, String apellido, String rut, int teléfono, String correo, int autosVendidos, String contraseña) {
        this.idVendedor = idVendedor;
        this.idSucursal = idSucursal;
        this.nombre = nombre;
        this.apellido = apellido;
        this.rut = rut;
        this.teléfono = teléfono;
        this.correo = correo;
        this.autosVendidos = autosVendidos;
        this.contraseña = contraseña;
    }

    public String getIdVendedor() {
        return idVendedor;
    }

    public void setIdVendedor(String idVendedor) {
        this.idVendedor = idVendedor;
    }

    public String getIdSucursal() {
        return idSucursal;
    }

    public void setIdSucursal(String idSucursal) {
        this.idSucursal = idSucursal;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public int getTeléfono() {
        return teléfono;
    }

    public void setTeléfono(int teléfono) {
        this.teléfono = teléfono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public int getAutosVendidos() {
        return autosVendidos;
    }

    public void setAutosVendidos(int autosVendidos) {
        this.autosVendidos = autosVendidos;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idVendedor != null ? idVendedor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vendedor)) {
            return false;
        }
        Vendedor other = (Vendedor) object;
        if ((this.idVendedor == null && other.idVendedor != null) || (this.idVendedor != null && !this.idVendedor.equals(other.idVendedor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "EntityClasses.Vendedor[ idVendedor=" + idVendedor + " ]";
    }
    
}

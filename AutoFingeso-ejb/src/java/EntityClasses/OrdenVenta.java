/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EntityClasses;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mauricio
 */
@Entity
@Table(name = "orden venta")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OrdenVenta.findAll", query = "SELECT o FROM OrdenVenta o"),
    @NamedQuery(name = "OrdenVenta.findByIdVenta", query = "SELECT o FROM OrdenVenta o WHERE o.idVenta = :idVenta"),
    @NamedQuery(name = "OrdenVenta.findByIdVendedor", query = "SELECT o FROM OrdenVenta o WHERE o.idVendedor = :idVendedor"),
    @NamedQuery(name = "OrdenVenta.findByIdCliente", query = "SELECT o FROM OrdenVenta o WHERE o.idCliente = :idCliente"),
    @NamedQuery(name = "OrdenVenta.findByIdAuto", query = "SELECT o FROM OrdenVenta o WHERE o.idAuto = :idAuto"),
    @NamedQuery(name = "OrdenVenta.findByMonto", query = "SELECT o FROM OrdenVenta o WHERE o.monto = :monto"),
    @NamedQuery(name = "OrdenVenta.findByFormaPago", query = "SELECT o FROM OrdenVenta o WHERE o.formaPago = :formaPago"),
    @NamedQuery(name = "OrdenVenta.findByDescuento", query = "SELECT o FROM OrdenVenta o WHERE o.descuento = :descuento")})
public class OrdenVenta implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "IdVenta")
    private String idVenta;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "IdVendedor")
    private String idVendedor;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "IdCliente")
    private String idCliente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "IdAuto")
    private String idAuto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Monto")
    private int monto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "FormaPago")
    private String formaPago;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Descuento")
    private int descuento;

    public OrdenVenta() {
    }

    public OrdenVenta(String idVenta) {
        this.idVenta = idVenta;
    }

    public OrdenVenta(String idVenta, String idVendedor, String idCliente, String idAuto, int monto, String formaPago, int descuento) {
        this.idVenta = idVenta;
        this.idVendedor = idVendedor;
        this.idCliente = idCliente;
        this.idAuto = idAuto;
        this.monto = monto;
        this.formaPago = formaPago;
        this.descuento = descuento;
    }

    public String getIdVenta() {
        return idVenta;
    }

    public void setIdVenta(String idVenta) {
        this.idVenta = idVenta;
    }

    public String getIdVendedor() {
        return idVendedor;
    }

    public void setIdVendedor(String idVendedor) {
        this.idVendedor = idVendedor;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getIdAuto() {
        return idAuto;
    }

    public void setIdAuto(String idAuto) {
        this.idAuto = idAuto;
    }

    public int getMonto() {
        return monto;
    }

    public void setMonto(int monto) {
        this.monto = monto;
    }

    public String getFormaPago() {
        return formaPago;
    }

    public void setFormaPago(String formaPago) {
        this.formaPago = formaPago;
    }

    public int getDescuento() {
        return descuento;
    }

    public void setDescuento(int descuento) {
        this.descuento = descuento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idVenta != null ? idVenta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrdenVenta)) {
            return false;
        }
        OrdenVenta other = (OrdenVenta) object;
        if ((this.idVenta == null && other.idVenta != null) || (this.idVenta != null && !this.idVenta.equals(other.idVenta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "EntityClasses.OrdenVenta[ idVenta=" + idVenta + " ]";
    }
    
}
